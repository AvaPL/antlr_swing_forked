grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

/*
  Zrealizowane:
  - akcje dla 4 działań
  - instrukcja drukująca wynik (print)
  - operacje dodatkowe: modulo i wartość bezwzględna
  - obsluga bloków instrukcji (za pomocą nawiasów klamrowych {})
  - obsługa zmiennych lokalnych i globalnych (deklaracje, odczyt, zapis)
*/

prog
    : (stat | blok | loop)+ EOF!;
    
loop: DO^ blok WHILE! LP! expr RP!
    ;

blok: LCB^ (stat | blok)+ RCB!
    ;

stat
    : expr NL -> expr

    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_stat NL -> if_stat
    | PRINT expr NL -> ^(PRINT expr)
    | NL ->
    ;

if_stat
    : IF^ expr THEN! (expr) (ELSE! (expr))?
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | MOD^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    | ABS expr ABS -> ^(ABS expr)
    ;

VAR :'var';

IF : 'if';
ELSE : 'else';
THEN : 'then';

DO: 'do';
WHILE: 'while';

PRINT :'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;
	
LCB
  : '{'
  ;
  
RCB
  : '}'
  ;
  
ABS
  : '|'
  ;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
MOD
  : '%'
  ;
