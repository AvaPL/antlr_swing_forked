tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;

import java.util.ArrayList;
import org.antlr.stringtemplate.StringTemplate;
import java.util.Collections;
}

@members {
  Integer numer = 0;
  ArrayList<StringTemplate> declarations = new ArrayList<StringTemplate>();
}

/*
  Zrealizowane (architektura rejestrowa):
  - obsługa zmiennych lokalnych, w tym deklaracje umieszczane przed "start"
  - if (wg filmu na YT)
  - pętla do while
*/

prog    : (e+=zakres | e+=expr | d+=decl | e+=loop)* {if ($d != null) declarations.addAll($d);} -> program(name={$e},deklaracje={declarations})
        ;

loop    : ^(DO z=zakres e=expr) {numer++;} -> do_while(blok={$z.st},war={$e.st},nr={numer.toString()})
        ;

zakres : ^(LCB {enterScope();} (e+=zakres | e+=expr | d+=decl)* {if ($d != null) declarations.addAll($d); leaveScope();}) -> blok(wyr={$e})
       ;

decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dek(n={$ID.text},depth={locals.currentDepth().toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e1=expr) -> podstaw(i1={$i1},e1={$e1.st},depth={locals.symbolDepth($i1.text).toString()})
        | INT                      -> int(i={$INT.text})
        | ID                       -> id(n={$ID.text},depth={locals.symbolDepth($ID.text).toString()})
        | ^(IF e1=expr e2=expr e3=expr?) {numer++;} -> if(e1={$e1.st},e2={$e2.st},e3={$e3.st},nr={numer.toString()})
    ;
    